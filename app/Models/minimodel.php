<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class minimodel extends Model
{
    protected $table = 'minitabla';

    protected $fillable = ['serievh', 'placa', 'placaant', 'estatustc', 'estatusg', 'notc', 'fecha_expedicion',
        'operador_mov', 'movimiento_t', 'modulo_t', 'revisor', 'propietario', 'folioteso', 'imptenencia', 'tipoplaca'];

    protected $primaryKey = 'id';

    public $timestamps = false;


}
