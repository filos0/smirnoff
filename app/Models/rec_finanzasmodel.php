<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class rec_finanzasmodel extends Model
{
    protected $connection = 'Informix';

    protected $table = 'rec_finanzas';

    protected $fillable = array('folioteso', 'serievh');


}
