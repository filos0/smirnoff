<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    protected $table = 'vehiculo';



    protected $fillable = ['serie_vehicular', 'modelo', 'numero_cilindros', 'numero_motor', 'origen_motor',
        'numero_personas', 'capacidad_litros', 'fecha_documento_legalizacion', 'pais_id', 'uso_vehiculo_id',
        'clase_tipo_vehiculo_id', 'tipo_combustible_id', 'capacidad_kwh', 'clave_vehicular_id', 'fecha_alta',
        'folio_documento_legalizacion', 'fecha_factura', 'numero_factura', 'importe_factura', 'numero_repuve',
        'aseguradora', 'numero_poliza_seguro', 'distribuidora', 'numero_puertas', 'tipo_servicio_id', 'estatus_id'];

    protected $primaryKey = 'id_vehiculo';

    /**
     * @param boolean $timestamps
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */


    public function clasetipo(){

        return $this->belongsTo('App\Models\Catalogos\Cat_clase_tipo_vehiculo', 'clase_tipo_vehiculo_id');
    }
    public function tiposervicio(){

        return $this->belongsTo('App\Models\Catalogos\CatServicioVehiculo', 'tipo_servicio_id');
    }
    public function uso(){

        return $this->belongsTo('App\Models\Catalogos\Cat_uso_vehiculo',  'uso_vehiculo_id');
    }
    public function clave_vehicular(){

        return $this->belongsTo('App\Models\Catalogos\Cat_clave_vehicular');
    }

    public function combustible(){

        return $this->belongsTo('App\Models\Catalogos\CatCombustibleVehiculo' ,'tipo_combustible_id' );
    }


    public function servicio_vehiculo(){

        return $this->belongsTo('App\Models\Catalogos\CatServicioVehiculo');
    }



    public function pais(){

        return $this->belongsTo('App\Models\Catalogos\Cat_pais');
    }

    public function tramites()
   {
       return $this->hasMany('App\Models\Tramite');
   }

   //////////////////////////////////////////////Mega tabla//////////////////////

    public function mega(){

        return $this->belongsTo('App\Models\MegatablaModel', 'serie_vehicular');

    }

/*


   public function facturacion()
   {
       return $this->belongsTo('App\Models\DatosFacturacion');
   }

   public function placa()
   {
       return $this->belongsToMany('App\Models\Placas', 'rmm_vehiculo_placa', 'id_vehiculo', 'id_placa' )->
       withPivot('fecha_inicio', 'fecha_fin', 'status');
   }

   public function linea()
   {
       return $this->belongsTo('App\Models\Catalogos\CatLinea');
   }

   public function marca()
   {
       return $this->belongsTo('App\Models\Catalogos\CatMarca');
   }*/
    /* public function circulacion()
   {
       return $this->hasMany('App\Models\DatosCirculacion');
   }*/

}
