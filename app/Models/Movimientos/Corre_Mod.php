<?php

namespace App\Models\Movimientos;

use Illuminate\Database\Eloquent\Model;

class Corre_Mod extends Model
{
    /**
     * The database connection used by the model.
     *
     * @var string
     */
    protected $connection = 'Informix';
    //
    protected $table='cvehcorrecciones';
    //protected $primaryKey = 'FOLIO';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable=[
        /**/
    ];

}
