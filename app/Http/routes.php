<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::any('/r/{a}', 'pruebasController@index');
Route::any('/', 'HomeController@index');


////////////////////////////////////////////////////////////////////
Route::any('/rdos/{a}/{b}', 'SdafaseController@index');

Route::any('/busca_linea/{linea}', 'SdafaseController@buscar_linea_captura');
Route::any('/busca_linea_finanzas/{serie}/{linea}', 'SdafaseController@buscar_linea_captura_finanzas');
//Route::any('/prro/{m}/{limimt}', 'PrroController@prro');
Route::POST('/prro/', 'PrroController@prro');

Route::any('/WebService/{placa}', 'WebServiceController@buscar_placa');
Route::any('/tramites/{placa}', 'WebServiceController@tramites');