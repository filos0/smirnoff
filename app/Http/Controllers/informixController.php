<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Models\Propietario as Propietario;
use App\Models\Vehiculo as Carro;
use App\Models\minimodel as Mini;
use App\Models\Cat_clave_vehicular as ClaveVehicular;


class informixController extends Controller
{

    public function insertar_Propietarios($propietarios)
    {
        //dd($propietarios);
        $a = count($propietarios);
        for ($i = 0; $i < $a; $i++) {
            $propietarios[$i] = get_object_vars($propietarios[$i]);
            $key = array_keys($propietarios[$i]);
            foreach ($key as $K) {
                $propietarios[$i][$K] = utf8_encode($propietarios[$i][$K]);
            }
        }

        //dd($propietarios);

        foreach ($propietarios as $propietario) {


            $clave_unica = utf8_encode($propietario['CLAVE_UNICA']);
          //  dd($clave_unica);
            $validar_cu = Propietario::where('clave_unica', $clave_unica)->count();
            if ($validar_cu == 0) {

                $nuevo_prop = Propietario::create([
                    'nombre_razon_social' => $propietario['NOMBRE_RAZON_SOCIAL'],
                    'primer_apellido' => $propietario['PRIMER_APELLIDO'],
                    'segundo_apellido' => $propietario['SEGUNDO_APELLIDO'],
                    'clave_unica' => $propietario['CLAVE_UNICA'],
                    'tipo_identificacion_id' => $propietario['TIPO_IDENTIFICACION_ID'],
                    'pais_id' => $propietario['PAIS_ID'],
                    'fecha_nacimiento' => $propietario['FECHA_NACIMIENTO'],
                    'telefono' => $propietario['TELEFONO'],
                    'tipo_propietario_id' => $propietario['TIPO_PROPIETARIO_ID'],
                    'id_colonia' => $propietario['ID_COLONIA']
                ]);


            }

        }
    }

    public function insertar_Vehiculos($vehiculo)
    {
        $vehiculo = array_change_key_case($vehiculo, CASE_LOWER);

        $a = array_keys($vehiculo);


        foreach ($a as $key) {

            $vehiculo[$key] = utf8_encode($vehiculo[$key]);
        }

        $serie = $vehiculo['serie_vehicular'];

        $nuevo_carro = Carro::create(array(
            'serie_vehicular' => $vehiculo['serie_vehicular'],
            'modelo' => $vehiculo['modelo'],
            'numero_cilindros' => $vehiculo['numero_cilindros'],
            'numero_motor' => $vehiculo['numero_motor'],
            'origen_motor' => $vehiculo['origen_motor'],
            'numero_personas' => $vehiculo['numero_personas'],
            'capacidad_litros' => $vehiculo['capacidad_litros'],
            'capacidad_kwh' => $vehiculo['capacidad_kwh'],
            'fecha_alta' => $vehiculo['fecha_alta'],
            'numero_puertas' => $vehiculo['numero_puertas'],
            'folio_documento_legalizacion' => $vehiculo['folio_documento_legalizacion'],
            'fecha_documento_legalizacion' => $vehiculo['fecha_documento_legalizacion'],
            'numero_factura' => $vehiculo['numero_factura'],
            'fecha_factura' => $vehiculo['fecha_factura'],
                'importe_factura' => $vehiculo['importe_factura'],
                'numero_repuve' => $vehiculo['numero_repuve'],
                'distribuidora' => $vehiculo['distribuidora'],
                'aseguradora' => $vehiculo['aseguradora'],
                'numero_poliza_seguro' => $vehiculo['numero_poliza_seguro'],
                'tipo_servicio_id' => $vehiculo['tipo_servicio_id'],
                'uso_vehiculo_id' => $vehiculo['uso_vehiculo_id'],
                'clase_tipo_vehiculo_id' => $vehiculo['clase_tipo_vehiculo_id'],
                'tipo_combustible_id' => $vehiculo['tipo_combustible_id'],
                'pais_id' => $vehiculo['pais_id'],
                'estatus_id' => $vehiculo['estatus_id'],
                'clave_vehicular_id' => $this->clv($vehiculo['clave_vehicular_id'])));



    }

    public function insertar_Tramite($tramites)
    {
        foreach ($tramites as $tramite) {
            $serie = $tramite->SERIEVH;
            $validar_tramite = Mini::where('serievh', $serie)->count();


            $nuevo_tramite = Mini::create([
                'serievh' => $tramite->SERIEVH,
                'placa' => $tramite->PLACA,
                'placaant' => $tramite->PLACAANT,
                'estatustc' => $tramite->ESTATUSTC,
                'estatusg' => $tramite->ESTATUSG,
                'notc' => $tramite->NOTC,
                'fecha_expedicion' => $tramite->FECHA_EXPEDICION,
                'operador_mov' => $tramite->OPERADOR_MOV,
                'movimiento_t' => $tramite->MOVIMIENTO_T,
                'modulo_t' => $tramite->MODULO_T,
                'revisor' => $tramite->REVISOR,
                'propietario' => Propietario::where('clave_unica', '=', $tramite->RFC)->first()->id_propietario,
                'folioteso' => $tramite->FOLIOTESO,
                'imptenencia' => $tramite->IMPTENENCIA,
                'tipoplaca' => $tramite->TIPOPLACA,
            ]);


        }
    }

    private function clv($clv){

        $clv_okla = ClaveVehicular::where('clave_vehicular', $clv)->first();
        if (!isset($clv_okla)) {
            $clv_fzn = $this->claveVehicular($clv);

            if ($clv_fzn == false) {
                return 1;
            }

            $data = (string)$clv_fzn->getBody()->getContents();
            $clv_fzn = json_decode($data, true);
            $marca = $clv_fzn['descripcionMarca'];
            $linea = $clv_fzn['descripcionLinea'];
            $version = $clv_fzn ['descripcionModelo'];
            $digito = $clv_fzn ['claveDigito'];

            if ($marca == 'null' && $linea == 'null' && $version == 'null' && $digito == 'null') {
                return 1;
            } else {

                $claveVeh = ClaveVehicular::create(['clave_vehicular' => $clv, 'digito' => $digito, 'marca' => $marca, 'linea' => $linea, 'version' => $version]);

                return $claveVeh->id_cat_clave_vehicular;

            }


        } else {
            return $clv_okla->id_cat_clave_vehicular;

        }

    }

    private function claveVehicular($numero)
    {
            $client = new Client;

            $r = $client->post('http://128.222.200.178:8252/ClaveVehicularFinanzas',
                ['json' => [
                    "numero" => $numero,
                ]]
            );

            if ($r->getStatusCode() != '200') {
                return false;
            }
            return $r;

    }



}