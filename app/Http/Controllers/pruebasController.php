<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\informixController as ins;
use App\Models\Vehiculo as carro;
use App\Models\Vehiculo as lineas;

class pruebasController extends Controller
{
    public function __construct(ins $ins, carro $carro, lineas $linea )
    {
        $this->ins = $ins;
        $this->carro = $carro;
        $this->linea = $linea;

    }

    //
    public function index($s)
    {
        $s = trim(strtoupper($s));

        $len = strlen($s);
       // dd($s);

        if ($len == 5 || $len == 6) {

            $serie = $this->placa($s);


            if (isset($serie)) {
                //foreach ($serie as $item){
                $this->busquedas($serie->SERIEVH);
                //}
            }

            return ['msg' => 'se busco '];

        } elseif ($len > 6 && $len <= 17) {

            $this->busquedas($s);

            return ['msg' => 1];

        } else {
            return ['msg' => 'no encontrado '.$s];
        }


    }

    private function busquedas($serie)
    {

        $vehiculo = $this->vehiculo($serie);
        $vehiculokla = carro::where('serie_vehicular', $serie)->get()->first();

        if (isset($vehiculo) && !isset($vehiculokla)) {

            $this->ins->insertar_Vehiculos($vehiculo);
            $propietarios = $this->propietario($serie);
            //dd($propietarios);
            if (isset($propietarios)) {

                $this->ins->insertar_Propietarios($propietarios);
            }


            $tramite = $this->tramite($serie);
            if (isset($tramite)) {

                $this->ins->insertar_Tramite($tramite);
            }

        }
    }

    private function placa($placa)
    {

        $placa_inx = DB::connection('Informix')->select('select first 1 serievh from cvehtarjetacir where placa="' . $placa . '"');

        if (isset($placa_inx[0])) {

            return $placa_inx[0];

        } else {
            return null;
        }

    }

    private function vehiculo($serie)
    {

        $vehiculo_inx = DB::connection('Informix')->select('select -- first  200
trim(serievh) as serie_vehicular,
modelo,
trim(numcil) as numero_cilindros,
trim(nummotor) as numero_motor,
"" origen_motor,
trim(numpasaje) as numero_personas,
litbat as capacidad_litros,
"" as capacidad_kwh,
fecalta as fecha_alta,
trim(numptas) as numero_puertas,
trim(foliolegal) as folio_documento_legalizacion,
feclegal as fecha_documento_legalizacion,
trim(numfactura) as numero_factura,
fecfactura as fecha_factura,
impfactura as importe_factura,
repuve as numero_repuve,
trim(nombredistrib) as distribuidora,
"" as aseguradora,
trim(numpoliza) as numero_poliza_seguro,
"1" as tipo_servicio_id,--cvetipserv
"1" as uso_vehiculo_id, --cveusovh
-- replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\'),
case
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'11\'     then   \'1\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'12\'     then   \'2\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'13\'     then   \'3\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'14\'     then   \'4\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'15\'     then   \'5\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'16\'     then   \'6\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'17\'     then   \'7\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'20\'     then   \'8\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'61\'     then   \'9\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'62\'     then   \'10\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'63\'     then   \'11\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'64\'     then   \'12\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'65\'     then   \'13\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'66\'     then   \'14\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'67\'     then   \'15\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'69\'     then   \'17\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'110\'    then   \'19\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'18\'     then   \'27\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'19\'     then   \'28\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'21\'     then   \'29\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'210\'    then   \'30\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'212\'    then   \'31\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'216\'    then   \'32\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'218\'    then   \'33\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'22\'     then   \'34\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'23\'     then   \'35\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'24\'     then   \'36\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'25\'     then   \'37\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'26\'     then   \'38\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'27\'     then   \'39\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'28\'     then   \'40\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'29\'     then   \'41\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'31\'     then   \'42\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'41\'     then   \'43\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'410\'    then   \'44\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'43\'     then   \'45\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'44\'     then   \'46\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'48\'     then   \'47\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'49\'     then   \'48\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'51\'     then   \'49\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'119\'     then   \'19\'
else null
end
as clase_tipo_vehiculo_id,--(cveclsvh||cvetipvh)

case when cvecomb = \'7\' then \'8\'
when cvecomb = \'8\' then \'9\'
else trim(cvecomb) end as tipo_combustible_id,
case when procedencia =\'N\' then 82 else 126 end as pais_id,
case when estatus =\'B\' then 2
when estatus=\'C\' then 2
else 1 end as estatus_id,
trim(cveveh) as clave_vehicular_id
from cvehgral 
where 
serievh="' . $serie . '"
order by fecalta desc');
        if (isset($vehiculo_inx[0])) {
//dd($vehiculo_inx);
            $vehiculo = array(
                'SERIE_VEHICULAR' => trim($vehiculo_inx[0]->SERIE_VEHICULAR),
                'MODELO' => trim($vehiculo_inx[0]->MODELO),
                'NUMERO_CILINDROS' => trim($vehiculo_inx[0]->NUMERO_CILINDROS),
                'NUMERO_MOTOR' => trim($vehiculo_inx[0]->NUMERO_MOTOR),
                'ORIGEN_MOTOR' => trim($vehiculo_inx[0]->ORIGEN_MOTOR),
                'NUMERO_PERSONAS' => trim($vehiculo_inx[0]->NUMERO_PERSONAS),
                'CAPACIDAD_LITROS' => trim($vehiculo_inx[0]->CAPACIDAD_LITROS),
                'CAPACIDAD_KWH' => trim($vehiculo_inx[0]->CAPACIDAD_KWH),
                'FECHA_ALTA' => trim($vehiculo_inx[0]->FECHA_ALTA),
                'NUMERO_PUERTAS' => trim($vehiculo_inx[0]->NUMERO_PUERTAS),
                'FOLIO_DOCUMENTO_LEGALIZACION' => trim($vehiculo_inx[0]->FOLIO_DOCUMENTO_LEGALIZACION),
                'FECHA_DOCUMENTO_LEGALIZACION' => trim($vehiculo_inx[0]->FECHA_DOCUMENTO_LEGALIZACION),
                'NUMERO_FACTURA' => trim($vehiculo_inx[0]->NUMERO_FACTURA),
                'FECHA_FACTURA' => trim($vehiculo_inx[0]->FECHA_FACTURA),
                'IMPORTE_FACTURA' => trim($vehiculo_inx[0]->IMPORTE_FACTURA),
                'NUMERO_REPUVE' => trim($vehiculo_inx[0]->NUMERO_REPUVE),
                'DISTRIBUIDORA' => trim($vehiculo_inx[0]->DISTRIBUIDORA),
                'ASEGURADORA' => trim($vehiculo_inx[0]->ASEGURADORA),
                'NUMERO_POLIZA_SEGURO' => trim($vehiculo_inx[0]->NUMERO_POLIZA_SEGURO),
                'TIPO_SERVICIO_ID' => trim($vehiculo_inx[0]->TIPO_SERVICIO_ID),
                'USO_VEHICULO_ID' => trim($vehiculo_inx[0]->USO_VEHICULO_ID),
                'CLASE_TIPO_VEHICULO_ID' => trim($vehiculo_inx[0]->CLASE_TIPO_VEHICULO_ID),
                'TIPO_COMBUSTIBLE_ID' => trim($vehiculo_inx[0]->TIPO_COMBUSTIBLE_ID),
                'PAIS_ID' => trim($vehiculo_inx[0]->PAIS_ID),
                'ESTATUS_ID' => trim($vehiculo_inx[0]->ESTATUS_ID),
                'CLAVE_VEHICULAR_ID' => trim($vehiculo_inx[0]->CLAVE_VEHICULAR_ID));
            return $vehiculo;
        } else
            return null;

    }

    private function propietario($serie)
    {
        $propietario_inx = DB::connection('Informix')->select('select	distinct 
\'\' id_propietario,
trim(NVL(
		tc.razsoc,
		tc.nombre
	)) nombre_razon_social,
	trim(tc.paterno) primer_apellido,
	trim(tc.materno) segundo_apellido,
	trim(tc.rfc) clave_unica,
	\'1\' tipo_identificacion_id,
	case
		when cvepais in(
			\'\',
			\'L\',
			\'110\',
			\'C\',
			\'N\',
			\'F\',
			\'109\'
		)
		or cvepais is null then \'82\'
		when cvepais = 1 then \'1\'
		when cvepais = 27 then \'17\'
		when cvepais = 79 then \'65\'
		when cvepais = 81 then \'67\'
		when cvepais = 175 then \'41\'
		when cvepais = 32 then \'22\'
		else \'82\'
	end pais_id,
tc.fecnac fecha_nacimiento,
-- \'\' sexo,
trim(telefono) telefono,
case  
when razsoc is not null then \'2\'else 
case when cvepais in (\'\',\'L\',\'110\',\'C\',\'N\',\'F\',\'109\') or cvepais is null then \'1\' else \'3\' 
end
end tipo_propietario_id,
\'1\' as  id_colonia
from
	cvehtarjetacir tc
	where serievh="' . $serie . '"');
        if (isset($propietario_inx[0])) {

            return $propietario_inx;
        } else
            return null;


    }

    private function tramite($serie)
    {

        $tramite_inx = DB::connection('Informix')->select('select

trim(t.serievh) serievh,
	trim(t.placa) placa,
	trim(t.placaant) placaant,

	trim(t.estatus) estatustc,
	trim(g.estatus) estatusg,
	trim(t.notc) notc,
	
	
	
	t.fecexped fecha_expedicion,
	
	
	trim(mov.cveusr) operador_mov,
	trim(mv.dsc) movimiento_t,
	trim(md.dsc) modulo_t,
	trim(t.revisor) revisor,
	trim(t.rfc) rfc,
	

	trim(af.folioteso) folioteso,
	af.imptenencia imptenencia,
	tp.dsc tipoplaca,
	\'\' id
	
from
cvehtarjetacir t   
left join
	cvehgral g   on    t.placa = g.placa  and 
	t.serievh=g.serievh
	 
 
left join
	cvehmodulos md on md.cvemod = t.cvemod 

left	  join
	cvehmov mov on  mov.placa = t.placa and t.notc = mov.notc
left	 join
	cvehmovto mv on mv.cvemovto = mov.cvemovto
	left join
	cvehantfis af on af.serievh=t.serievh and t.notc=af.notc and af.placa=t.placa and af.cvemovto=mv.cvemovto
	left join cvehtipoplaca tp on tp.tipoplaca=g.tipoplaca
where t.serievh ="' . $serie . '"
order by
	t.fecexped');

        if (isset($tramite_inx[0])) {
            return $tramite_inx;
        } else
            return null;

    }




}