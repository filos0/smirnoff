<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\informixController as ins;
use App\Models\Vehiculo as carro;
use App\Models\cvehantfismodel as lineas;


class Lineas_capturaController extends Controller
{
    public function __construct(ins $ins, carro $carro, lineas $linea)
    {
        $this->ins = $ins;
        $this->carro = $carro;
        $this->linea = $linea;
    }

    //
    public function index($s)
    {
        dd($s);
        $serie = strtoupper($s->serie);
        $linea = strtoupper($s->line);
        $consulta=$this->consultalinea($serie);
        if( isset($consulta[0])){
            if ($consulta[0]->LINEA == $linea)
                return 1;
            else
                return 0;
        }
        else
            return null;


    }

    private function consultalinea($s){

        $vehiculo_inx = DB::connection('Informix')->
        select('select trim(serievh) as serie, trim(folioteso) as linea from rec_finanzas where serievh="'.$s.' limit 1"');

        return $vehiculo_inx;


    }

    public function buscar_linea_captura($linea){
        $placa_inx = lineas::where('folioteso', '=', $linea)->get();
dd($placa_inx);
        if (empty($placa_inx) ){
            return "NO_EXISTE";
        }
        else{
            return "EXISTE";
        }
    }




    //
}

