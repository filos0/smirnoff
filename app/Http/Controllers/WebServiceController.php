<?php

namespace App\Http\Controllers;

use DB;

class WebServiceController extends Controller
{


    public function buscar_placa($placa)
    {
        $serie = DB::connection('Informix')->select('select first 1 serievh from cvehtarjetacir where placa="'.$placa.'"');

        if (empty($serie)) {
            return null;
        }
        else{
            $vehiculo = $this->vehiculo(trim(strtoupper($serie[0]->SERIEVH)));
            $tramite = $this->tramite(trim(strtoupper($serie[0]->SERIEVH)));
            $propietario = $this->propietario(trim(strtoupper($serie[0]->SERIEVH)));
            return
                array(["placa"=>$placa,
                        "vehciulo" => $vehiculo,
                        "tramite" => $tramite,
                        "propietario"=> $propietario]);
        }


    }
	
	  public function tramites($placa)
    {
        $serie = DB::connection('Informix')->select('select first 1 serievh from cvehtarjetacir where placa="'.$placa.'"');

        if (empty($serie)) {
            return null;
        }
        else{
            return $tramite = $this->tramite(trim(strtoupper($serie[0]->SERIEVH)));
                
        }

    }

    public function vehiculo($serie)
    {

        $vehiculo_inx = DB::connection('Informix')->select('select -- first  200
trim(serievh) as serie_vehicular,
modelo,
trim(numcil) as numero_cilindros,
trim(nummotor) as numero_motor,
"" origen_motor,
trim(numpasaje) as numero_personas,
litbat as capacidad_litros,
"" as capacidad_kwh,
fecalta as fecha_alta,
trim(numptas) as numero_puertas,
trim(foliolegal) as folio_documento_legalizacion,
feclegal as fecha_documento_legalizacion,
trim(numfactura) as numero_factura,
fecfactura as fecha_factura,
impfactura as importe_factura,
repuve as numero_repuve,
trim(nombredistrib) as distribuidora,
"" as aseguradora,
trim(numpoliza) as numero_poliza_seguro,
"1" as tipo_servicio_id,--cvetipserv
"1" as uso_vehiculo_id, --cveusovh
-- replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\'),
case
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'11\'     then   \'1\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'12\'     then   \'2\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'13\'     then   \'3\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'14\'     then   \'4\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'15\'     then   \'5\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'16\'     then   \'6\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'17\'     then   \'7\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'20\'     then   \'8\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'61\'     then   \'9\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'62\'     then   \'10\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'63\'     then   \'11\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'64\'     then   \'12\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'65\'     then   \'13\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'66\'     then   \'14\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'67\'     then   \'15\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'69\'     then   \'17\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'110\'    then   \'19\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'18\'     then   \'27\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'19\'     then   \'28\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'21\'     then   \'29\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'210\'    then   \'30\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'212\'    then   \'31\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'216\'    then   \'32\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'218\'    then   \'33\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'22\'     then   \'34\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'23\'     then   \'35\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'24\'     then   \'36\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'25\'     then   \'37\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'26\'     then   \'38\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'27\'     then   \'39\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'28\'     then   \'40\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'29\'     then   \'41\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'31\'     then   \'42\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'41\'     then   \'43\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'410\'    then   \'44\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'43\'     then   \'45\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'44\'     then   \'46\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'48\'     then   \'47\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'49\'     then   \'48\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'51\'     then   \'49\'
when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'119\'     then   \'19\'
else null
end
as clase_tipo_vehiculo_id,--(cveclsvh||cvetipvh)

case when cvecomb = \'7\' then \'8\'
when cvecomb = \'8\' then \'9\'
else trim(cvecomb) end as tipo_combustible_id,
case when procedencia =\'N\' then 82 else 126 end as pais_id,
case when estatus =\'B\' then 2
when estatus=\'C\' then 2
else 1 end as estatus_id,
trim(cveveh) as clave_vehicular_id
from cvehgral 
where 
serievh="' . $serie . '"
order by fecalta desc');
        if (isset($vehiculo_inx[0])) {
            return $vehiculo_inx[0];
        } else
            return null;

    }
    public function tramite($serie)
    {
        $tramite_inx = DB::connection('Informix')->select('select

trim(t.serievh) serievh,
	trim(t.placa) placa,
	trim(t.placaant) placaant,

	trim(t.estatus) estatustc,
	trim(g.estatus) estatusg,
	trim(t.notc) notc,
	
	
	
	t.fecexped fecha_expedicion,
	
	
	trim(mov.cveusr) operador_mov,
	trim(mv.dsc) movimiento_t,
	trim(md.dsc) modulo_t,
	trim(t.revisor) revisor,
	trim(t.rfc) rfc,
	

	trim(af.folioteso) folioteso,
	af.imptenencia imptenencia,
	tp.dsc tipoplaca,
	\'\' id
	
from
cvehtarjetacir t   
left join
	cvehgral g   on    t.placa = g.placa  and 
	t.serievh=g.serievh
	 
 
left join
	cvehmodulos md on md.cvemod = t.cvemod 

left	  join
	cvehmov mov on  mov.placa = t.placa and t.notc = mov.notc
left	 join
	cvehmovto mv on mv.cvemovto = mov.cvemovto
	left join
	cvehantfis af on af.serievh=t.serievh and t.notc=af.notc and af.placa=t.placa and af.cvemovto=mv.cvemovto
	left join cvehtipoplaca tp on tp.tipoplaca=g.tipoplaca
where t.serievh ="' . $serie . '"
order by
	t.fecexped');

        if (isset($tramite_inx[0])) {
            return array_pop($tramite_inx);
        } else
            return null;

    }
    public function propietario($serie)
    {
        $propietario_inx = DB::connection('Informix')->select('select	distinct 
\'\' id_propietario,
trim(NVL(
		tc.razsoc,
		tc.nombre
	)) nombre_razon_social,
	trim(tc.paterno) primer_apellido,
	trim(tc.materno) segundo_apellido,
	trim(tc.rfc) clave_unica,
	\'1\' tipo_identificacion_id,
	case
		when cvepais in(
			\'\',
			\'L\',
			\'110\',
			\'C\',
			\'N\',
			\'F\',
			\'109\'
		)
		or cvepais is null then \'82\'
		when cvepais = 1 then \'1\'
		when cvepais = 27 then \'17\'
		when cvepais = 79 then \'65\'
		when cvepais = 81 then \'67\'
		when cvepais = 175 then \'41\'
		when cvepais = 32 then \'22\'
		else \'82\'
	end pais_id,
tc.fecnac fecha_nacimiento,
-- \'\' sexo,
trim(telefono) telefono,
case  
when razsoc is not null then \'2\'else 
case when cvepais in (\'\',\'L\',\'110\',\'C\',\'N\',\'F\',\'109\') or cvepais is null then \'1\' else \'3\' 
end
end tipo_propietario_id,
\'1\' as  id_colonia
from
	cvehtarjetacir tc
	where serievh="' . $serie . '"');
        if (isset($propietario_inx[0])) {

            return $propietario_inx[0];
        } else
            return null;


    }

}