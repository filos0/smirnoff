<?php

namespace App\Http\Controllers\pruebas;

use DB;
use App\Http\Controllers\Controller;
use Auth;

class pruebasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function index()
    {
        if (Auth::user()->hasRole(['admin']))
        {

            try
            {

                $fecha_act = date("m" . '-' . "d" . '-' . "Y");
                $niv = "3N1EB31S76K306389";
                $vehiculo_inx = DB::connection('Informix')->select('
                select trim(serievh) as serie_vehicular, modelo, trim(numcil) as numero_cilindros, trim(nummotor) as numero_motor, "" origen_motor, trim(numpasaje) as numero_personas,
                litbat as capacidad_litros, "" as capacidad_kwh, fecalta as fecha_alta, trim(numptas) as numero_puertas, trim(foliolegal) as folio_documento_legalizacion,
                feclegal as fecha_documento_legalizacion, trim(numfactura) as numero_factura, fecfactura as fecha_factura, impfactura as importe_factura, repuve as numero_repuve,
                trim(nombredistrib) as distribuidora, "" as aseguradora, trim(numpoliza) as numero_poliza_seguro, "1" as tipo_servicio_id, "1" as uso_vehiculo_id,
                case
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'11\'     then   \'1\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'12\'     then   \'2\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'13\'     then   \'3\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'14\'     then   \'4\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'15\'     then   \'5\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'16\'     then   \'6\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'17\'     then   \'7\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'20\'     then   \'8\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'61\'     then   \'9\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'62\'     then   \'10\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'63\'     then   \'11\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'64\'     then   \'12\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'65\'     then   \'13\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'66\'     then   \'14\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'67\'     then   \'15\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'69\'     then   \'17\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'110\'    then   \'19\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'18\'     then   \'27\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'19\'     then   \'28\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'21\'     then   \'29\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'210\'    then   \'30\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'212\'    then   \'31\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'216\'    then   \'32\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'218\'    then   \'33\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'22\'     then   \'34\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'23\'     then   \'35\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'24\'     then   \'36\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'25\'     then   \'37\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'26\'     then   \'38\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'27\'     then   \'39\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'28\'     then   \'40\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'29\'     then   \'41\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'31\'     then   \'42\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'41\'     then   \'43\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'410\'    then   \'44\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'43\'     then   \'45\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'44\'     then   \'46\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'48\'     then   \'47\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'49\'     then   \'48\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'51\'     then   \'49\'
                when replace((trim(cveclsvh)||trim(cvetipvh)),\'10\',\'19\') =      \'119\'     then   \'19\'
                else null
                end
                as clase_tipo_vehiculo_id,--(cveclsvh||cvetipvh)                
                case when cvecomb = \'7\' then \'8\'
                when cvecomb = \'8\' then \'9\'
                else trim(cvecomb) end as tipo_combustible_id, case when procedencia =\'N\' then 82 else 126 end as pais_id, case when estatus =\'B\' then 2
                when estatus=\'C\' then 3
                else 1 end as estatus_id,
                id_cat_clave_vehicular as clave_vehicular_id
                from cvehgral inner join suarveclaves on trim(cveveh)=clave_vehicular
                
                where
                serievh=\'3N1EB31S76K306389\' order by fecalta desc ');
                //dd($vehiculo_inx[0]);
                try
                {
                        $ins_vehiculo= DB::connection('90bd')->table('vehiculo')->insert(
                            [
                                'SERIE_VEHICULAR' => trim($vehiculo_inx[0]->SERIE_VEHICULAR),
                                'MODELO' => trim($vehiculo_inx[0]->MODELO),
                                'NUMERO_CILINDROS' => trim($vehiculo_inx[0]->NUMERO_CILINDROS),
                                'NUMERO_MOTOR' => trim($vehiculo_inx[0]->NUMERO_MOTOR),
                                'ORIGEN_MOTOR' => trim($vehiculo_inx[0]->ORIGEN_MOTOR),
                                'NUMERO_PERSONAS' => trim($vehiculo_inx[0]->NUMERO_PERSONAS),
                                'CAPACIDAD_LITROS' => trim($vehiculo_inx[0]->CAPACIDAD_LITROS),
                                'CAPACIDAD_KWH' => trim($vehiculo_inx[0]->CAPACIDAD_KWH),
                                'FECHA_ALTA' => trim($vehiculo_inx[0]->FECHA_ALTA),
                                'NUMERO_PUERTAS' => trim($vehiculo_inx[0]->NUMERO_PUERTAS),
                                'FOLIO_DOCUMENTO_LEGALIZACION' => trim($vehiculo_inx[0]->FOLIO_DOCUMENTO_LEGALIZACION),
                                'FECHA_DOCUMENTO_LEGALIZACION' => trim($vehiculo_inx[0]->FECHA_DOCUMENTO_LEGALIZACION),
                                'NUMERO_FACTURA' => trim($vehiculo_inx[0]->NUMERO_FACTURA),
                                'FECHA_FACTURA' => trim($vehiculo_inx[0]->FECHA_FACTURA),
                                'IMPORTE_FACTURA' => trim($vehiculo_inx[0]->IMPORTE_FACTURA),
                                'NUMERO_REPUVE' => trim($vehiculo_inx[0]->NUMERO_REPUVE),
                                'DISTRIBUIDORA' => trim($vehiculo_inx[0]->DISTRIBUIDORA),
                                'ASEGURADORA' => trim($vehiculo_inx[0]->ASEGURADORA),
                                'NUMERO_POLIZA_SEGURO' => trim($vehiculo_inx[0]->NUMERO_POLIZA_SEGURO),
                                'TIPO_SERVICIO_ID' => trim($vehiculo_inx[0]->TIPO_SERVICIO_ID),
                                'USO_VEHICULO_ID' => trim($vehiculo_inx[0]->USO_VEHICULO_ID),
                                'CLASE_TIPO_VEHICULO_ID' => trim($vehiculo_inx[0]->CLASE_TIPO_VEHICULO_ID),
                                'TIPO_COMBUSTIBLE_ID' => trim($vehiculo_inx[0]->TIPO_COMBUSTIBLE_ID),
                                'PAIS_ID' => trim($vehiculo_inx[0]->PAIS_ID),
                                'ESTATUS_ID' => trim($vehiculo_inx[0]->ESTATUS_ID),
                                'CLAVE_VEHICULAR_ID' => trim($vehiculo_inx[0]->CLAVE_VEHICULAR_ID),

                            ]);

                    dd('Se realizo la inserción correctamente');
                }
                catch (\Exception $e)
                {
                    dd('Error: '.$e->getMessage().' '. $e->getLine());
                }

            }

        catch (\Exception $e)
        {
            dd('Error: '.$e->getMessage().' '. $e->getLine());
        }
        }

        else
        {
            $error = 'No cuenta con los privilegios suficientes para acceder a esta pagina';
            return view('home')->withErrors($error);
        }
    }
}