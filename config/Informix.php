<?php

return [
    'Informix' => [
        'driver' => 'informix',
        'host' => env('DB_HOST', '128.222.200.15'),
        'port' => env('DB_PORT', '1526'),
        'database' => env('DB_DATABASE', 'ctrlpart_sarve'),
        'username' => env('DB_USERNAME', 'informix'),
        'password' => env('DB_PASSWORD', '54rv3p4rt'),
        'server' => env('DB_SERVER', 'ol_part_sarve'),
        'protocol' => env('DB_PROTOCOL', 'onsoctcp'), # opcional
    ],
];
