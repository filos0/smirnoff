<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inicio de sesión</title>

    <!-- Global stylesheets -->
    <link href="{{ asset('/assets/fonts/font.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{asset('assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/login.js')}}"></script>
    <!-- /theme JS files -->

</head>

<body class="login-container login-cover">

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content pb-20">

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="panel panel-body login-form">
                        <div class="text-center">

                            <h5 class="content-group cdmx-text" ><img src="{{URL::asset('/assets/images/logo_semovi_cdmx_498_75px.png')}}" alt="SEMOVI" width="250px"><br><br>Control Vehicular y Otros Estados</h5>
                        </div>
                        <br>
                        <div class="form-group has-feedback has-feedback-left ">
                            <input  id="email" type="email" class="form-control" name="email" placeholder="Email" autofocus autocomplete="false">
                            @if ($errors->has('email'))
                                <span class="text-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                            @endif

                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left ">
                            <input  id="password" type="password" class="form-control" name="password" placeholder="Contraseña">

                            @if ($errors->has('password'))
                                <span class="text-danger">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                            @endif

                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                    <!-- <div class="form-group login-options">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" class="styled" checked="checked">
                                        Recuerdame
                                    </label>
                                </div>

                                <div class="col-sm-6 text-right">
                                    <a href="{{ url('/password/reset') }}">¿Se te olvido la contraseña?</a>
                                </div>
                            </div>
                        </div>-->

                        <div class="form-group">
                            <br>

                            <button type="submit" class="btn bg-pink-400 btn-block">Entrar <i class="icon-circle-right2 position-right"></i></button>
                        </div>
                    <!--
                        <div class="content-divider text-muted form-group"><span>¿No tienes una cuenta ?</span></div>
                        <a href="{{ url('/register') }}" class="btn bg-slate btn-block content-group">Registrar</a>
-->
                    </div>
                </form>



            </div>


        </div>


    </div>

</div>
</body>
</html>