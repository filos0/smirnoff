<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta id="csrf_token" name="csrf_token" content="{{ csrf_token() }}">
    <title>Semovi::@yield('title')</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/assets/css/colors.css') }}" rel="stylesheet" type="text/css">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ URL::asset('/assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/plugins/loaders/blockui.min.js') }}"></script>

    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ URL::asset('/assets/js/plugins/forms/wizards/stepy.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/core/libraries/jasny_bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/pages/wizard_stepy.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/pages/form_bootstrap_select.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>


    <!-- /theme JS files -->
    <!-- Core JS files -->
    <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="assets/js/plugins/notifications/bootbox.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>

    <!-- /theme JS files -->
    <script type="text/javascript" src="assets/js/pages/components_modals.js"></script>
    <!-- Theme JS files -->
    <script type="text/javascript" src="assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/datatables_basic.js"></script>

    <!-- Scrip Calendario  --->
    <link rel="stylesheet" type="text/css" href="../calendario/tcal.css" />
    <script type="text/javascript" src="../calendario/tcal.js"></script>


</head>
        <body>
              <div class="navbar navbar-default header-highlight">


                  <div class="navbar-header">
                      <a class="navbar-text text-center text-white">Proyecto Eve133</a>

                        <ul class="nav navbar-nav visible-xs-block">
                          <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                          <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                        </ul>
                  </div>

                  <div class="navbar-collapse collapse" id="navbar-mobile">
                      <ul class="nav navbar-nav">
                        <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

                          <div class="navbar-right">
                            <p class=" navbar-text text-center ">Secretaría de Movilidad&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<script>
var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
var f=new Date();
document.write(diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
</script></p>
                          </div>

                      </ul>
                  </div>
              </div>
<!-- /main navbar -->


              <!-- Page container -->
              <div class="page-container">
                    <!-- Page content -->
                    <div class="page-content">
                        <!-- Main sidebar -->
                        <div class="sidebar sidebar-main">

                            <div class="sidebar-content">

                                <div class="navbar-header">
                                    <ul class="nav navbar-nav visible-xs-block">
                                      <li><a data-toggle="collapse" data-target="#navbar-mobile" class="legitRipple"><i class="icon-tree5"></i></a></li>
                                      <li><a class="sidebar-mobile-main-toggle legitRipple"><i class="icon-paragraph-justify3"></i></a></li>
                                    </ul>
                                </div>


              <div class="sidebar-category sidebar-category-visible">

                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            @role(['admin','usr_m'])
                                                        <li class="">

                                                            <a href="#" class="has-ul legitRipple"><i class="icon-file-check"></i>
                                                                <span>Realizar Altas de Otros Estados</span></a>
                                                            <ul class="hidden-ul" style="display: none;">

                                                              <li class="">
                                                                  <a href="{{ url('/autorizacion_pagos') }}" name="url_buscar" class="legitRipple"><i
                                                                              class="icon-file-plus"></i>
                                                                      <span>Altas para Automóviles de Otros Estados</span></a>
                                                              </li>

                                                              <li class="">
                                                                  <a href="{{ url('/cancelar_tram') }}" name="url_buscar" class="legitRipple"><i
                                                                              class="icon-user-block"></i>
                                                                      <span>Cancelación de Tramites de Otros Estados</span></a>
                                                              </li>

                                                            <!--   <li class="">
                                                                  <a href="{{ url('/unificar_tram') }}" name="url_buscar" class="legitRipple"><i
                                                                              class="icon-users2"></i>
                                                                      <span>Excepciones NIV o Placa</span></a>
                                                              </li>

                                                            <!--   <li class="">
                                                                  <a href="{{ url('/busqueda_tram') }}" name="url_buscar" class="legitRipple"><i
                                                                              class="icon-search4"></i>
                                                                      <span>Busqueda de Tramites</span></a>
                                                              </li> -->

                                                            </ul>
                                                        </li>
                            @endrole

                            @role(['admin','usr_ext'])
                            <li class="">

                                <a href="#" class="has-ul legitRipple"><i class="icon-vcard"></i>
                                    <span>Realizar Altas Extranjeras</span></a>
                                <ul class="hidden-ul" style="display: none;">

                                    <li class="">
                                        <a href="{{ url('/autorizacion_pagos_ext') }}" name="url_buscar" class="legitRipple"><i
                                                    class="icon-file-plus"></i>
                                            <span>Altas para Automóviles Extranjeros</span></a>
                                    </li>

                                    <li class="">
                                        <a href="{{ url('/cancelar_tram_ext') }}" name="url_buscar" class="legitRipple"><i
                                                    class="icon-user-block"></i>
                                            <span>Cancelación de Tramites de Automóviles Extranjeros</span></a>
                                    </li>

                                <!--   <li class="">
                                                                  <a href="{{ url('/unificar_tram') }}" name="url_buscar" class="legitRipple"><i
                                                                              class="icon-users2"></i>
                                                                      <span>Excepciones NIV o Placa</span></a>
                                                              </li>

                                                            <!--   <li class="">
                                                                  <a href="{{ url('/busqueda_tram') }}" name="url_buscar" class="legitRipple"><i
                                                                              class="icon-search4"></i>
                                                                      <span>Busqueda de Tramites</span></a>
                                                              </li> -->

                                </ul>
                            </li>
                            @endrole

                            @role(['admin','usr_h'])
                                                        <li class="">

                                                            <a href="#" class="has-ul legitRipple"><i class="icon-magazine"></i>
                                                                <span>Historial de Movimientos</span></a>
                                                            <ul class="hidden-ul" style="display: none;">

                                                              <li class="">
                                                                  <a href="{{ url('/buscar_mov') }}" name="url_buscar" class="legitRipple"><i
                                                                              class="icon-vcard"></i>
                                                                      <span>Consulta de Movimientos</span></a>
                                                              </li>

                                                            </ul>
                                                        </li>
                        @endrole



                            @role(['admin','seg_t'])
                            <li class="">

                                <a href="#" class="has-ul legitRipple"><i class="icon-folder-search"></i>
                                    <span>Seguimiento de Altas Foraneas</span></a>
                                <ul class="hidden-ul" style="display: none;">

                                    <li class="">
                                        <a href="{{ url('/seguimientos') }}" name="url_buscar" id="url_buscar" class="legitRipple"><i
                                                    class="icon-vcard"></i>
                                            <span>Consulta de Altas Foraneas</span></a>
                                    </li>

                                </ul>
                            </li>
                        @endrole

                            @role(['admin','seg_t'])
                            <li class="">

                                <a href="#" class="has-ul legitRipple"><i class="icon-folder-search"></i>
                                    <span>Seguimiento de Altas Extranjeras</span></a>
                                <ul class="hidden-ul" style="display: none;">

                                    <li class="">
                                        <a href="{{ url('/seguimientos_ext') }}" name="url_buscar" id="url_buscar" class="legitRipple"><i
                                                    class="icon-vcard"></i>
                                            <span>Consulta de Altas Extranjeras</span></a>
                                    </li>

                                </ul>
                            </li>
                        @endrole

                           <!--
                            <li class="">

                                <a href="#" class="has-ul legitRipple"><i class="icon-folder-search"></i>
                                    <span>PDFAMORFO</span></a>
                                <ul class="hidden-ul" style="display: none;">

                                    <li class="">
                                        <a href="{{ url('/pdfamorfo') }}" name="url_buscar_ext" id="url_buscar_ext" class="legitRipple">
                                            <i class="icon-vcard"></i>
                                            <span>pdfamorfo</span></a>
                                    </li>

                                </ul>
                            </li>
                        -->



                                                        <!-- <li class="">
                                                            <a href="{{ url('/liberar_pago') }}" class="legitRipple">
                                                                <i class="icon-cash3"></i>
                                                                <span>Consulta de Pago Utilizado</span></a>
                                                        </li> -->

                        								<li><a href="{{ url('/logout') }}"><i class="icon-switch2"></i>Cerrar Sesión</a></li></li>


                        </ul>
                       <!--Aqui acaba ul -->
                    </div>
              </div>
                <!-- /main navigation -->

                                </div>
                        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                @yield('content')

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

                      </div>
    <!-- /page content -->

              </div>
<!-- /page container -->

      </body>

</html>
