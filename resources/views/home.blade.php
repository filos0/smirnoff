@extends('template.master')

@section('content')

    <!-- /theme JS files -->
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Bienvenido</div>

                <div class="panel-body">
                    <div style='margin-left: 200px;'><img src="pdf/semovi_header.png"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
